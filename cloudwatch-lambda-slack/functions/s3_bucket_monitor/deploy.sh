#!/bin/bash
set -ex

# Create a .zip of src
pushd src
zip -r ../src.zip *
popd

# aws s3api create-bucket 
# --bucket aws-lambda-functions-artifacts 
# --region eu-west-2 
# --create-bucket-configuration LocationConstraint=eu-west-2

# http://aws-lambda-functions-artifacts.s3.amazonaws.com/
aws s3 cp src.zip s3://aws-lambda-functions-artifacts/ec2-state-change/src.zip
version=$(aws s3api head-object --bucket aws-lambda-functions-artifacts --key ec2-state-change/src.zip)
version=$(echo $version | python -c 'import json,sys; obj=json.load(sys.stdin); print(obj["VersionId"])')

# set -ex;echo '{"name":"abdul"}' |python -c 'import json,sys; obj=json.load(sys.stdin); print(obj["name"])'
# echo '{"name":"abdul"}' | python -c 'import json,sys; obj=json.load(sys.stdin); print(obj["name"])'
# echo '{"name":"abdul"}' | python -c 'import base64,sys; print(sys.stdin);obj=base64.b64encode(sys.stdin); print(obj)'

# echo '{"name":"abdul"}' | python -c 'import base64,sys; print(sys.stdin);obj=base64.b64encode(sys.stdin); print(obj)'