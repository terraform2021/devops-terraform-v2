# Resources
# (1) cloudwatch - Event Rule
# (2) cloudwatch - Event Target
# (3) IAM Role for giving access to Lambda
# (4) Role Policy for cloudWatch Logging
# (5) Create Lambda function
# (6) Cloudwatch Lambda Permissions

##############################################################
# aws_cloudwatch_event_rule
# aws_cloudwatch_event_target

# aws_iam_role
# aws_iam_role_policy

# aws_lambda_function
# aws_lambda_permission
##############################################################


# Create Rule (CloudWatch Event)

# Create Role (IAM) for lambda (execution role) 
#                   with trust policy with principle 'lambda.amazonaws.com'
# Create Policies (permission policies) for lambda role

# Lambda function and give lambda role

# Create Rule Target: giving lambda arn as target

# lambda invoke Permissions to cloudwatch_event

########################### TODO #########################
# Lambda function - provile role-arn and bucket-name-and-key
# Event_Target (provide rule-name & target=lambda-arn)
# Give permission to Rule Event to invoke lambda function
# Give Role Policies

# (1) Event Rule
resource "aws_cloudwatch_event_rule" "rule" {
  name        = "${var.cloudwatch_event_rule_name}"
  description = "${var.cloudwatch_event_rule_description}"

  event_pattern = "${var.cloudwatch_event_rule_pattern}"
}

# (3) IAM Role for for Lambda execution
resource "aws_iam_role" "lambda" {
  name = "${var.lambda_iam_role_name}"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}


# (5) Create Lambda function
resource "aws_lambda_function" "lambda" {
  function_name = "${var.lambda_function_name}"
  role          = "${aws_iam_role.lambda.arn}"
  handler       = "${var.lambda_handler}"
  runtime       = "${var.lambda_runtime}"

  s3_bucket         = "${var.lambda_artifacts_bucket_name}"
  s3_key            = "${var.lambda_artifacts_bucket_key}"
  s3_object_version = "${var.lambda_version}"

  environment {
        variables = "${var.lambda_environment}"
  }
}

# (2) Event Target : Rule and Lambda function
resource "aws_cloudwatch_event_target" "target" {
  rule      = "${aws_cloudwatch_event_rule.rule.name}"  
  arn       = "${aws_lambda_function.lambda.arn}"
}


# aws_iam_role
# aws_iam_role_policy
# aws_iam_policy_document

data "aws_iam_policy_document" "lambda_s3_external" {
  statement {
    effect  = "Allow"
    actions = [ "s3:GetObject",
                "s3:ListBucketMultipartUploads",
                "s3:ListBucketVersions",
                "s3:ListBucket",
                "s3:ListMultipartUploadParts",
                "s3:ListStorageLensConfigurations",
                "s3:ListAllMyBuckets",
                "s3:ListJobs"]
                
    resources = ["arn:aws:s3:::*"]
  }
}

data "aws_iam_policy_document" "lambda_cloudwatch_external" {
  statement {
    effect  = "Allow"
    actions = [ "logs:CreateLogGroup",
                "logs:CreateLogStream",
                "logs:PutLogEvents"]

    resources = ["*"]
  }
}

resource "aws_iam_role_policy" "lambda_cloudwatch_logging" {
  name = "lambda-cloudwatch-logging"
  role = "${aws_iam_role.lambda.id}"
  policy = data.aws_iam_policy_document.lambda_cloudwatch_external.json
}

resource "aws_iam_role_policy" "lambda_s3_access" {
  name = "lambda-s3-access"
  role = "${aws_iam_role.lambda.id}"
  policy = data.aws_iam_policy_document.lambda_s3_external.json
}

# (4) Role Policy for cloudWatch Logging
/*
resource "aws_iam_role_policy" "lambda_cloudwatch_logging" {
  name = "lambda-cloudwatch-logging"
  role = "${aws_iam_role.lambda.id}"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogGroup",
                "logs:CreateLogStream",
                "logs:PutLogEvents"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}

# (4) Role Policy for S3 Access
resource "aws_iam_role_policy" "lambda_s3_access" {
  name = "lambda-s3-access"
  role = "${aws_iam_role.lambda.id}"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "s3:GetObject",
                "s3:ListBucketMultipartUploads",
                "s3:ListBucketVersions",
                "s3:ListBucket",
                "s3:ListMultipartUploadParts"
            ],
            "Resource": "arn:aws:s3:::*"
        },
        {
            "Sid": "VisualEditor1",
            "Effect": "Allow",
            "Action": [
                "s3:ListStorageLensConfigurations",
                "s3:ListAllMyBuckets",
                "s3:ListJobs"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}
*/

# (6)  Cloudwatch Lambda Permissions
resource "aws_lambda_permission" "cloudwatch_lambda_execution" {
  statement_id  = "AllowExecutionFromCloudWatch"
  
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.lambda.function_name}"
  
  principal     = "events.amazonaws.com"
  source_arn    = "${aws_cloudwatch_event_rule.rule.arn}"
}
