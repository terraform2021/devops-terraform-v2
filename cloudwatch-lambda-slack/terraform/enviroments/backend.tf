terraform {
  /*
  backend "s3" {
    bucket         = "aws-health-notif-demo-tfstate"
    key            = "terraform.tfstate"
    region         = "eu-west-2"
    #dynamodb_table = "aws-health-notif-demo-tfstate"
  }*/

  backend "local" {
    path = "terraform.tfstate"
  }
}
