terraform {
  /*
  backend "s3" {
    bucket         = "aws-health-notif-demo-tfstate"
    key            = "terraform.tfstate"
    region         = "eu-west-2"
    #dynamodb_table = "aws-health-notif-demo-tfstate"
  }*/
  
 required_version = ">= 0.12.26"

  required_providers {
    aws    = ">= 3.36"
    random = ">= 2.0"
  }

  backend "local" {
    path = "terraform.tfstate"
  }
}
